package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;
import javafx.scene.layout.Background;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Controller {

    @FXML
    private Button button00;
    @FXML
    private Button button01;
    @FXML
    private Button button02;
    @FXML
    private Button button03;
    @FXML
    private Button button04;
    @FXML
    private Button button05;
    @FXML
    private Button button06;
    @FXML
    private Button button07;
    @FXML
    private Button button08;
    @FXML
    private Button button09;
    @FXML
    private Button button10;
    @FXML
    private Button button11;

    private ButtonStatus[] buttonStatuses;
    private Button[] buttons;
    public Controller() {
        String[] colors = {"633974", "FEF9E7", "5DADE2", "1C2833", "F7DC6F", "EC7063", "A9CCE3"};
        buttonStatuses = new ButtonStatus[12];
        for (int i = 0, y = 0; i < 6; i++, y+=2){
            buttonStatuses[y] = new ButtonStatus();
            buttonStatuses[y].color = colors[i];
            buttonStatuses[y+1] = new ButtonStatus();
            buttonStatuses[y+1].color = colors[i];
        }
        shuffleArray(buttonStatuses);
        for (int i = 0; i < 12 ; i++){
            buttonStatuses[i].index = i;
        }
    }

    static void shuffleArray(ButtonStatus[] ar)
    {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            ButtonStatus a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    private int prevId = 0;

    @FXML
    private void onClickMethod(ActionEvent event) {
        buttons = new Button[]{button00, button01,button02,button03,button04,button05,button06,button07,button08,button09,button10,button11};

        var id = getButtonId(event);
        if (buttonStatuses[id].isConstShow)
        {
            return;
        }
        if (buttonStatuses[id].isShow)
        {
            hideColorOnButton(id);
            return;
        }
        else {
            showColorOnButton(id);
        }

        if (buttonStatuses[prevId].color == buttonStatuses[id].color && prevId != id)
        {
            fixColorOnButton(prevId, id);
            return;
        }
        if ( prevId != id) {
            hideColorOnButton(prevId);
        }
        prevId = id;
    }

    private int getButtonId(ActionEvent event)
    {
        var button = (Button)event.getTarget();
        var idString = button.getId().replace("button", "");
        var id = Integer.parseInt(idString);
        return id;
    }

    private void hideColorOnButton(int id)
    {
        if (buttonStatuses[id].isConstShow)
            return;
        buttonStatuses[id].isShow = false;
        var backGroundStyle = String.format("-fx-background-color: #eff;");
        buttons[id].setStyle(backGroundStyle);
    }

    private void fixColorOnButton(int prevId, int id)
    {
        buttonStatuses[prevId].isConstShow = true;
        buttonStatuses[id].isConstShow = true;
        buttonStatuses[prevId].isShow = false;
        buttonStatuses[id].isShow = false;
        buttons[id].setText("Fixed");
        buttons[prevId].setText("Fixed");
    }

    private void showColorOnButton(int id)
    {
        buttonStatuses[id].isShow = true;
        var backGroundStyle = String.format("-fx-background-color: #%s;",buttonStatuses[id].color);
        buttons[id].setStyle(backGroundStyle);
    }
}

class ButtonStatus {
    public String color;
    public boolean isShow;
    public boolean isConstShow;
    public int index;
}
